cmake_minimum_required(VERSION 3.8.2)

#-----------------------------------------------------------------------------
# Plugin name and version

project(rgg-session VERSION 1.0)

#-----------------------------------------------------------------------------
# Plugin options

option(RGG_ENABLE_PYTHON_WRAPPING "Build Python Wrappings" ON)
option(ENABLE_CUBIT_BINDINGS "Enable meshing with Cubit >=15.1")
option(ENABLE_MCC3_BINDINGS "Enable cross section generation with MCC3 (require PyARC)")

if (ENABLE_CUBIT_BINDINGS)
  # For now, require that the user provide paths to coregen, assygen and cubit
  set(ASSYGEN_EXE "${CMAKE_INSTALL_PREFIX}/meshkit/bin/assygen" CACHE PATH
    "Path to assygen executable")
  set(COREGEN_EXE "${CMAKE_INSTALL_PREFIX}/meshkit/bin/coregen" CACHE PATH
    "Path to coregen executable")
  set(CUBIT_EXE "${CMAKE_INSTALL_PREFIX}/bin/cubit" CACHE PATH
    "Path to cubit executable")
endif()

if (ENABLE_MCC3_BINDINGS)
  set(MCC3_EXE "${CMAKE_INSTALL_PREFIX}/bin/mcc3.x" CACHE PATH
    "Path to mcc3 executable")
endif()

# enforce shared libs - otherwise we don't get a loadable plugin
set(BUILD_SHARED_LIBS ON)

#-----------------------------------------------------------------------------
# Find packages for the plugin
find_package(smtk REQUIRED)
find_package(Boost 1.64.0 REQUIRED COMPONENTS filesystem)

if (NOT SMTK_ENABLE_PARAVIEW_SUPPORT)
  message(FATAL_ERROR
    "This project requires SMTK to be built with ParaView support.")
endif()
find_package(Qt5 COMPONENTS Core)

if (RGG_ENABLE_PYTHON_WRAPPING)
  find_package(Python3 REQUIRED
    COMPONENTS Development)

  # Set the python module extension (needed for pybind11)
  if(MSVC)
    set(PYTHON_MODULE_EXTENSION ".pyd")
  else()
    set(PYTHON_MODULE_EXTENSION ".so")
  endif()

  find_package(pybind11 REQUIRED)
endif ()

#-----------------------------------------------------------------------------
# Boost settings
if(NOT DEFINED Boost_USE_STATIC_LIBS)
  if(${BUILD_SHARED_LIBS})
    set(Boost_USE_STATIC_LIBS OFF)
  else()
    set(Boost_USE_STATIC_LIBS ON)
  endif()
endif()

add_library(boost_cxx_flags INTERFACE)
# For MSVC:
#  (/EHsc) setup windows exception handling
#  (/wd4996) quiet warnings about printf being potentially unsafe
#  (/wd4503) quiet warnings about truncating decorated name
#  (-DBOOST_ALL_NO_LIB) remove autolinking
target_compile_options(boost_cxx_flags INTERFACE
  $<$<CXX_COMPILER_ID:MSVC>:/EHsc /wd4996 /wd4503 -DBOOST_ALL_NO_LIB>)

set_property(TARGET Boost::boost APPEND PROPERTY INTERFACE_LINK_LIBRARIES boost_cxx_flags)

#-----------------------------------------------------------------------------
# Python settings

# Initialize PYTHON_MODULEDIR.
# This stores the location where we'll install our Python modules.
# Note that PYTHON_MODULEDIR may be provided to override this behavior.
if (NOT DEFINED PYTHON_MODULEDIR)
  if (INSTALL_PYTHON_TO_SITE_PACKAGES)
    execute_process(
      COMMAND
      ${Python3_EXECUTABLE}
      -c "import site; print(site.getsitepackages())[-1]"
      RESULT_VARIABLE PYTHON_MODULEDIR
      )
  elseif(WIN32)
    set(PYTHON_MODULEDIR
      "bin/Lib/site-packages")
  else()
    set(PYTHON_MODULEDIR
      "lib/python${Python3_VERSION_MAJOR}.${Python3_VERSION_MINOR}/site-packages")
  endif()
endif()

#-----------------------------------------------------------------------------
# Pybind11 settings

set(PYBIND11_FLAGS " ")
if(CMAKE_CXX_COMPILER_ID MATCHES "Clang" OR
    CMAKE_CXX_COMPILER_ID MATCHES "GNU" OR
    CMAKE_CXX_COMPILER_ID MATCHES "Intel")
  set(PYBIND11_FLAGS "${PYBIND11_FLAGS} -Wno-shadow")
endif()

#-----------------------------------------------------------------------------
# CMake build settings

# Set the directory where the binaries will be stored
include(GNUInstallDirs)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_BINDIR})
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR})
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR})
set(CMAKE_INSTALL_CMAKEDIR "${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}")


# Add our Cmake directory to the module search path
list(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/CMake)

# Add optional path for installing neutronics-workflow files.
# This would typically be set by superbuild.
set(SIMULATION_WORKFLOWS_ROOT "share/cmb/workflows" CACHE PATH
  "Install directory for neutronics-workflow files")

#-----------------------------------------------------------------------------
# Plugin tests

option(ENABLE_TESTING "Enable Testing" OFF)
if (ENABLE_TESTING)
  include(CTest)
  include(TestingMacros)
endif()

#-----------------------------------------------------------------------------
# Plugin sources

add_subdirectory(smtk)
# Include the plugin's source directory
# Populate the plugin files list, scan and build them
set(plugin_files
  smtk/session/rgg/plugin/paraview.plugin)
if (ENABLE_PYTHON_WRAPPING)
  list(APPEND plugin_files
    smtk/simulation/proteus/plugin/paraview.plugin)
endif()

option(ENABLE_PYARC_BINDINGS "Enable PyARC bindings" OFF)
if (ENABLE_PYARC_BINDINGS)
  list(APPEND plugin_files
    smtk/simulation/pyarc/plugin/paraview.plugin)
endif ()

paraview_plugin_scan(
  PLUGIN_FILES ${plugin_files}
  PROVIDES_PLUGINS paraview_plugins
  ENABLE_BY_DEFAULT ON
  HIDE_PLUGINS_FROM_CACHE ON)

set(autoload_args AUTOLOAD)

option(ENABLE_PLUGIN_BY_DEFAULT "Automatically enable the plugin in CMB modelbuilder" OFF)
if (ENABLE_PLUGIN_BY_DEFAULT)
  list(APPEND autoload_args ${paraview_plugins})
endif ()


string(REPLACE "-" "_" safe_project_name "${PROJECT_NAME}")

paraview_plugin_build(
  LIBRARY_SUBDIRECTORY "smtk-${smtk_VERSION}"
  PLUGINS ${paraview_plugins}
  PLUGINS_FILE_NAME "smtk.rggsession.xml"
  ${autoload_args}
  INSTALL_EXPORT ${PROJECT_NAME}-plugin
  HEADERS_DESTINATION  "${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}"
  CMAKE_DESTINATION "${CMAKE_INSTALL_CMAKEDIR}"
  ADD_INSTALL_RPATHS ON
  TARGET ${safe_project_name}_paraview_plugins)

include(CMakePackageConfigHelpers)

# Our requirements for a version file are basic, so we use CMake's basic version
# file generator
write_basic_package_version_file(
  "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}-config-version.cmake"
  VERSION ${${PROJECT_NAME}_VERSION}
  COMPATIBILITY AnyNewerVersion
)

export(
  EXPORT      ${PROJECT_NAME}
  FILE        "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_CMAKEDIR}/${PROJECT_NAME}-targets.cmake")
install(
  EXPORT      ${PROJECT_NAME}
  DESTINATION "${CMAKE_INSTALL_CMAKEDIR}"
  FILE        "${PROJECT_NAME}-targets.cmake")


configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/CMake/smtk-plugin-config.cmake.in"
  "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_CMAKEDIR}/${PROJECT_NAME}-config.cmake"
  @ONLY)
install(
  FILES       "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_CMAKEDIR}/${PROJECT_NAME}-config.cmake"
  DESTINATION "${CMAKE_INSTALL_CMAKEDIR}")
