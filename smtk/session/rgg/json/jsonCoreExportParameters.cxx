//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/rgg/json/jsonCoreExportParameters.h"

#include<iostream>

namespace smtk
{
namespace session
{
namespace rgg
{
using json = nlohmann::json;

SMTKRGGSESSION_EXPORT void to_json(
  nlohmann::json& j, const CoreExportParameters& aep)
{
  j["geometry"] = aep.Geometry;
  j["geometryType"] = aep.GeometryType;
  j["geomEngine"] = aep.GeomEngine;
  j["extrude"] = aep.Extrude;
  if (aep.MergeTolerance >= 0)
  {
    j["mergeTolerance"] = aep.MergeTolerance;
  }
  j["neumannSet"] = aep.NeumannSet;
  j["outputFileName"] = aep.OutputFileName;
  j["backgroundInfo"] = aep.BackgroundInfo;
  j["problemType"] = aep.ProblemType;
  j["saveParallel"] = aep.SaveParallel;
  j["info"] = aep.Info;
  j["meshINFO"] = aep.MeshINFO;
  j["symmetry"] = aep.Symmetry;
}

SMTKRGGSESSION_EXPORT void from_json(
  const nlohmann::json& j, CoreExportParameters& aep)
{
  try
  {
    aep.Geometry = j.at("geometry").get<std::string>();
    aep.GeometryType = j.at("geometryType").get<std::string>(); // Hexagonal or Rectangular
    aep.GeomEngine = j.at("geomEngine").get<std::string>();
    aep.Extrude = j.at("extrude").get<std::string>();
    if (j.find("mergeTolerance") != j.end())
    {
      aep.MergeTolerance = j.at("mergeTolerance");
    }
    json neumannSetJson = j.at("neumannSet");
    for (json::iterator it = neumannSetJson.begin(); it != neumannSetJson.end();++it)
    {
      aep.NeumannSet.push_back(*it);
    }
    aep.OutputFileName = j.at("outputFileName").get<std::string>();
    aep.BackgroundInfo = j.at("backgroundInfo");
    aep.ProblemType = j.at("problemType").get<std::string>();
    aep.SaveParallel = j.at("saveParallel").get<std::string>();
    aep.Info = j.at("info").get<std::string>();
    aep.MeshINFO = j.at("meshINFO").get<std::string>();
    aep.Symmetry = j.at("symmetry").get<std::string>();
  }
  catch
  (json::exception& e)
  {
    std::cerr << e.what() <<std::endl;
    std::cerr << "Failed to parse the CoreExportParameter in the core" <<std::endl;
  }
}

} // namespace rgg
} // namespace session
} // namespace smtk
