//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_rgg_json_jsonPin_h
#define smtk_session_rgg_json_jsonPin_h

#include "smtk/session/rgg/Exports.h"

#include "smtk/session/rgg/Pin.h"

#include "nlohmann/json.hpp"

namespace smtk
{
namespace session
{
namespace rgg
{

// Define how rgg pin and its subparts are de/serialized.
SMTKRGGSESSION_EXPORT void to_json(
  nlohmann::json& j, const Pin::Piece& piece);

SMTKRGGSESSION_EXPORT void from_json(
  const nlohmann::json& j, Pin::Piece& piece);

SMTKRGGSESSION_EXPORT void to_json(
  nlohmann::json& j, const Pin::LayerMaterial& lm);

SMTKRGGSESSION_EXPORT void from_json(
  const nlohmann::json& j, Pin::LayerMaterial& lm);

SMTKRGGSESSION_EXPORT void to_json(
  nlohmann::json& j, const Pin& pin);

SMTKRGGSESSION_EXPORT void from_json(
  const nlohmann::json& j, Pin& pin);

} // namespace rgg
} // namespace session
} // namespace smtk
#endif
