//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_rgg_json_jsonCoreExportParameters_h
#define smtk_session_rgg_json_jsonCoreExportParameters_h

#include "smtk/session/rgg/Exports.h"

#include "smtk/session/rgg/meshkit/CoreExportParameters.h"

#include "nlohmann/json.hpp"

namespace smtk
{
namespace session
{
namespace rgg
{

SMTKRGGSESSION_EXPORT void to_json(
    nlohmann::json& j, const CoreExportParameters& cep);

SMTKRGGSESSION_EXPORT void from_json(
    const nlohmann::json& j, CoreExportParameters& aep);

} // namespace rgg
} // namespace session
} // namespace smtk

#endif
