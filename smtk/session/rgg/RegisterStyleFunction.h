//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_rgg_RegisterStyleFunction_h
#define smtk_session_rgg_RegisterStyleFunction_h

#include "smtk/session/rgg/Exports.h"

#include "smtk/extension/paraview/server/vtkSMTKRepresentationStyleGenerator.h"
#include "smtk/extension/paraview/server/vtkSMTKResourceRepresentation.h"

namespace smtk
{
namespace session
{
namespace rgg
{

class SMTKRGGSESSION_EXPORT RegisterStyleFunction : public vtkSMTKRepresentationStyleSupplier<RegisterStyleFunction>
{
public:
  bool valid(const smtk::resource::ResourcePtr& in) const override
  {
    return true;
  }

  StyleFromSelectionFunction operator()(const smtk::resource::ResourcePtr& in) override
  {
    // for RGG, we don't want to show the selection in the 3D render view,
    // so return a lambda that does nothing.
    return [](smtk::view::SelectionPtr /*seln*/,
  vtkSMTKResourceRepresentation::RenderableDataMap& /*renderables*/, vtkSMTKResourceRepresentation* /*self*/) {
      return true;
    };
  }
};
}
}
}

#endif // smtk_session_rgg_RegisterStyleFunction_h
