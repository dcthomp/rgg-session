//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_session_rgg_Pin_h
#define pybind_smtk_session_rgg_Pin_h

#include <pybind11/pybind11.h>

#include "smtk/session/rgg/Pin.h"
#include "smtk/model/AuxiliaryGeometry.h"
#include "smtk/session/rgg/json/jsonPin.h"
#include <pybind11/stl.h>

namespace py = pybind11;

py::class_< smtk::session::rgg::Pin > pybind11_init_smtk_session_rgg_Pin(py::module &m)
{
  py::class_< smtk::session::rgg::Pin > instance(m, "Pin");
  instance
    .def(py::init<>())
    .def(py::init<::std::string const &, ::std::string const &>())
    .def(py::init<::smtk::session::rgg::Pin const &>())
    .def(py::init([](smtk::model::AuxiliaryGeometry& pinAux)
        { return nlohmann::json::parse(pinAux.stringProperty(smtk::session::rgg::Pin::propDescription)[0]);}))
    .def("__eq__", (bool (smtk::session::rgg::Pin::*)(::smtk::session::rgg::Pin const &) const) &smtk::session::rgg::Pin::operator==)
    .def("__ne__", (bool (smtk::session::rgg::Pin::*)(::smtk::session::rgg::Pin const &) const) &smtk::session::rgg::Pin::operator!=)
    .def("deepcopy", (smtk::session::rgg::Pin & (smtk::session::rgg::Pin::*)(::smtk::session::rgg::Pin const &)) &smtk::session::rgg::Pin::operator=)
    .def_static("isAnUniqueLabel", &smtk::session::rgg::Pin::isAnUniqueLabel, py::arg("label"))
    .def_static("generateUniqueLabel", &smtk::session::rgg::Pin::generateUniqueLabel)
    .def("name", &smtk::session::rgg::Pin::name)
    .def("label", &smtk::session::rgg::Pin::label)
    .def("cellMaterialIndex", &smtk::session::rgg::Pin::cellMaterialIndex)
    .def("color", &smtk::session::rgg::Pin::color)
    .def("isCutAway", &smtk::session::rgg::Pin::isCutAway)
    .def("zOrigin", &smtk::session::rgg::Pin::zOrigin)
    .def("pieces", (std::vector<smtk::session::rgg::Pin::Piece, std::allocator<smtk::session::rgg::Pin::Piece> > & (smtk::session::rgg::Pin::*)()) &smtk::session::rgg::Pin::pieces)
    .def("pieces", (std::vector<smtk::session::rgg::Pin::Piece, std::allocator<smtk::session::rgg::Pin::Piece> > const & (smtk::session::rgg::Pin::*)() const) &smtk::session::rgg::Pin::pieces)
    .def("layerMaterials", (std::vector<smtk::session::rgg::Pin::LayerMaterial, std::allocator<smtk::session::rgg::Pin::LayerMaterial> > & (smtk::session::rgg::Pin::*)()) &smtk::session::rgg::Pin::layerMaterials)
    .def("layerMaterials", (std::vector<smtk::session::rgg::Pin::LayerMaterial, std::allocator<smtk::session::rgg::Pin::LayerMaterial> > const & (smtk::session::rgg::Pin::*)() const) &smtk::session::rgg::Pin::layerMaterials)
    .def("setName", &smtk::session::rgg::Pin::setName, py::arg("name"))
    .def("setLabel", &smtk::session::rgg::Pin::setLabel, py::arg("label"))
    .def("setCellMaterialIndex", &smtk::session::rgg::Pin::setCellMaterialIndex, py::arg("index"))
    .def("setColor", &smtk::session::rgg::Pin::setColor, py::arg("color"))
    .def("setCutAway", &smtk::session::rgg::Pin::setCutAway, py::arg("cutAway"))
    .def("setZOrigin", &smtk::session::rgg::Pin::setZOrigin, py::arg("zOrigin"))
    .def("setPieces", &smtk::session::rgg::Pin::setPieces, py::arg("pieces"))
    .def("setLayerMaterials", &smtk::session::rgg::Pin::setLayerMaterials, py::arg("lm"))
    .def_property_readonly_static("propDescription", [](py::object){ return std::string(smtk::session::rgg::Pin::propDescription);})
    .def_property_readonly_static("typeDescription", [](py::object){ return std::string(smtk::session::rgg::Pin::typeDescription);})
    .def_property_readonly_static("subpartDescription", [](py::object){ return std::string(smtk::session::rgg::Pin::subpartDescription);})
    .def_property_readonly_static("layerDescription", [](py::object){ return std::string(smtk::session::rgg::Pin::layerDescription);})
    .def_property_readonly_static("materialDescription", [](py::object){ return std::string(smtk::session::rgg::Pin::materialDescription);})
    ;
  py::enum_<smtk::session::rgg::Pin::PieceType>(instance, "PieceType")
    .value("CYLINDER", smtk::session::rgg::Pin::PieceType::CYLINDER)
    .value("FRUSTUM", smtk::session::rgg::Pin::PieceType::FRUSTUM)
    .export_values();
  py::class_< smtk::session::rgg::Pin::Piece >(instance, "Piece")
    .def(py::init<>())
    .def(py::init<::smtk::session::rgg::Pin::PieceType, double, double, double>())
    .def(py::init<::smtk::session::rgg::Pin::Piece const &>())
    .def("deepcopy", (smtk::session::rgg::Pin::Piece & (smtk::session::rgg::Pin::Piece::*)(::smtk::session::rgg::Pin::Piece const &)) &smtk::session::rgg::Pin::Piece::operator=)
    .def_readwrite("pieceType", &smtk::session::rgg::Pin::Piece::pieceType)
    .def_readwrite("length", &smtk::session::rgg::Pin::Piece::length)
    .def_readwrite("baseRadius", &smtk::session::rgg::Pin::Piece::baseRadius)
    .def_readwrite("topRadius", &smtk::session::rgg::Pin::Piece::topRadius)
    ;
  py::class_< smtk::session::rgg::Pin::LayerMaterial >(instance, "LayerMaterial")
    .def(py::init<>())
    .def(py::init<int, double>())
    .def(py::init<::smtk::session::rgg::Pin::LayerMaterial const &>())
    .def("deepcopy", (smtk::session::rgg::Pin::LayerMaterial & (smtk::session::rgg::Pin::LayerMaterial::*)(::smtk::session::rgg::Pin::LayerMaterial const &)) &smtk::session::rgg::Pin::LayerMaterial::operator=)
    .def_readwrite("subMaterialIndex", &smtk::session::rgg::Pin::LayerMaterial::subMaterialIndex)
    .def_readwrite("normRadius", &smtk::session::rgg::Pin::LayerMaterial::normRadius)
    ;
  return instance;
}

#endif
