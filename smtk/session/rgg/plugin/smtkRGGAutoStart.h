//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_extension_paraview_appcomponents_pqSMTKAppComponentsAutoStart_h
#define smtk_extension_paraview_appcomponents_pqSMTKAppComponentsAutoStart_h

#include "smtk/session/rgg/plugin/Exports.h"

#include <QObject>

class vtkSMProxy;
class smtkRGGBehavior;

class smtkRGGAutoStart : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

  static vtkSMProxy* resourceManager();
public:
  smtkRGGAutoStart(QObject* parent = nullptr);
  ~smtkRGGAutoStart() override;

  void startup();
  void shutdown();
protected:
  smtkRGGBehavior* m_Tracker;
private:
  Q_DISABLE_COPY(smtkRGGAutoStart);
};

#endif
