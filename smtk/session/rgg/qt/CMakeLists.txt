find_package(Qt5 REQUIRED COMPONENTS Core Widgets)

# We need to add the current value of VTK_MODULES_DIR to the module path
# so that when the plugins are built all the modules can be found. Otherwise,
# modules that aren't loaded as direct dependencies of CMB modules will
# not be found.
list(APPEND CMAKE_MODULE_PATH "${VTK_MODULES_DIR}")

set (srcs
  qtColorButton.cxx
  qtDraw2DLattice.cxx
  qtDrawLatticeItem.cxx
  qtLattice.cxx
  qtLatticeHelper.cxx
)

set (non_ui_srcs
  rggLatticeContainer.cxx
  rggNucAssembly.cxx
  rggNucCore.cxx
  rggNucCoordinateConverter.cxx
)

set (hdrs
  qtColorButton.h
  qtDraw2DLattice.h
  qtLattice.h
  qtLatticeHelper.h
  rggNucAssembly.h
  rggNucCore.h
  rggLatticeContainer.h
  rggNucCoordinateConverter.h
  rggNucPartDefinition.h
)


set(CMAKE_AUTOMOC 1)

add_library(smtkQtRGGSession SHARED
  ${srcs}
  ${non_ui_srcs}
  ${hdrs}
)

#we need to add the location of the moc files to the include dir for smtkQtRGGSession
target_include_directories(smtkQtRGGSession PRIVATE ${CMAKE_CURRENT_BINARY_DIR})

#set smtkQtRGGSession to publicly link to smtkCore
target_link_libraries(smtkQtRGGSession
  LINK_PUBLIC
    smtkQtExt
    smtkCore
    Qt5::Core
    Qt5::Widgets
LINK_PRIVATE
    smtkRGGSession
    ${VTK_LIBRARIES}
)

generate_export_header(smtkQtRGGSession EXPORT_FILE_NAME Exports.h)

smtk_get_kit_name(name dir_prefix)

# Install the header files
install(
  FILES
    ${hdrs}
    ${CMAKE_CURRENT_BINARY_DIR}/Exports.h
  DESTINATION
    ${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}/${dir_prefix})

# Install the library and exports
install(
  TARGETS smtkQtRGGSession
  EXPORT  ${PROJECT_NAME}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}/${dir_prefix})

add_subdirectory(nuclides)
