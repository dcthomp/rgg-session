//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef __smtk_session_rgg_meshkit_AssyExportParameters_h
#define __smtk_session_rgg_meshkit_AssyExportParameters_h

#include "smtk/model/FloatData.h"
#include "smtk/model/IntegerData.h"
#include "smtk/session/rgg/Exports.h"

#include <tuple>
#include <set>
#include <string>


namespace smtk
{
namespace session
{
namespace rgg
{

/**\brief Assembly export parameters used by meshkit
  * For parameters which can be put in the common.inp file, currently there is no
  * ducumentation for it and you can only find Impl details in
  * <meshkit_src>/src/algs/Assygen/Assygen.cpp.
  * Explananation of keyword used by assygen:
  * https://press3.mcs.anl.gov/sigma/meshkit/rgg/assygen-input-file-keyword-definitions/
  */
struct SMTKRGGSESSION_EXPORT AssyExportParameters
{
  // a string used to fetch json rep from string property
  static constexpr const char* const propDescription = "export_parameters";

  AssyExportParameters();

  // TODO: Allow developers to initiaize these common parameters at core creation/edit time.
  // Params which can be put in common.inp. See more options in Assygen.cpp file
  std::string GeomEngine;
  int StartPinId;
  std::string MeshType; // hex or Tet
  std::string Info;
  std::tuple<int, double, double> HBlock;
  std::string GeometryType; // Hexagonal or Rectangular
  std::string Geometry;
  std::string CreateSideset;
  std::string CreateFiles;
  std::string SaveExodus;
  double MergeTolerance;
  double RadialMeshSize;
  double TetMeshSize;
  double AxialMeshSize;
  double EdgeInterval;
  std::string MeshScheme;

  // Assembly specific params
  std::string Rotate;
  bool Center;
  std::string Section;
  std::string Move;
  int NeumannSet_StartId;
  int MaterialSet_StartId;
  std::string Superblocks;
  int NumSuperBlocks;
  int List_MaterialSet_StartId;
  int List_NeumannSet_StartId;

  // Params which must be placed in each inp file
private:
  void init();
};

} // namespace rgg
} // namespace session
} // namespace smtk

#endif // __smtk_session_rgg_meshkit_AssyExportParameters_h
