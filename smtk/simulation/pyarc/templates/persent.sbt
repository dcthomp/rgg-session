<?xml version="1.0"?>
<SMTK_AttributeSystem Version="2">

  <Definitions>

    <AttDef Type="dif3d" BaseType="" Unique="true" Associations="">

      <ItemDefinitions>

        <Group Name="persent" Label="Persent" AdvanceLevel="0">

          <ItemDefinitions>

            <Group Name="pert_calc" Label="Perturbation Calculations" AdvanceLevel="0">

              <DetailedDescription>
                This group is used for perturbation theory calculations. It requires to
                define a perturbed state for the core considered to calculate the
                difference in reactivity with the reference state.
              </DetailedDescription>

              <ItemDefinitions>

	        <String Name="pert_id" Label="ID">
                  <DiscreteInfo DefaultIndex="0">
                    <Value Enum="A">A</Value>
                    <Value Enum="B">B</Value>
                    <Value Enum="C">C</Value>
                    <Value Enum="D">D</Value>
                    <Value Enum="E">E</Value>
                    <Value Enum="F">F</Value>
                    <Value Enum="G">G</Value>
                    <Value Enum="H">H</Value>
                    <Value Enum="I">I</Value>
                    <Value Enum="J">J</Value>
                    <Value Enum="K">K</Value>
                    <Value Enum="L">L</Value>
                    <Value Enum="M">M</Value>
                    <Value Enum="N">N</Value>
                    <Value Enum="O">O</Value>
                    <Value Enum="P">P</Value>
                    <Value Enum="Q">Q</Value>
                    <Value Enum="R">R</Value>
                    <Value Enum="S">S</Value>
                    <Value Enum="T">T</Value>
                    <Value Enum="U">U</Value>
                    <Value Enum="V">V</Value>
                    <Value Enum="W">W</Value>
                    <Value Enum="X">X</Value>
                    <Value Enum="Y">Y</Value>
                    <Value Enum="Z">Z</Value>
                  </DiscreteInfo>
	        </String>

                <Int Name="pert_depletion_step" Label="Depletion Step">
                  <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
                  <DefaultValue>1</DefaultValue>
                </Int>

	        <String Name="pert_type" Label="Perturbation Type">
                  <DiscreteInfo DefaultIndex="0">
                    <Value Enum="keff">keff</Value>
                    <Value Enum="lambda_beta">lambda_beta</Value>
                  </DiscreteInfo>
	        </String>

                <String Name="density_perturbation_factor_or_new_temp"
                        Label="Density Perturbation Factor or New Temperature"
                        Optional="true" IsEnabledByDefault="false">

                  <ChildrenDefinitions>

                    <Double Name="density_perturbation_factor"
                            Label="Density Perturbation Factor">
                      <BriefDescription>factor by which the density of the
                      isotopes perturbed will be multiplied</BriefDescription>
                      <RangeInfo><Min Inclusive="false">0</Min></RangeInfo>
                      <DefaultValue>1</DefaultValue>
                    </Double>

                    <Double Name="new_temp"
                            Label="New Temperature">
                      <BriefDescription>new temperature of the isotopes perturbed</BriefDescription>
                      <RangeInfo><Min Inclusive="false">0</Min></RangeInfo>
                      <DefaultValue>1</DefaultValue>
                    </Double>

	            <Void Name="pert_xs_updated" Label="Upate Cross Sections"
                          Optional="true" IsEnabledByDefault="true">
                  <BriefDescription>indicate whether cross-sections
                  will be recalculated for the perturbed state</BriefDescription>
                    </Void>

                  </ChildrenDefinitions>

                  <DiscreteInfo DefaultIndex="0">
	            <Structure>
                      <Value Enum="Density Perturbation Factor">density_perturbation_factor</Value>
	              <Items>
		        <Item>density_perturbation_factor</Item>
		        <Item>xs_updated</Item>
	              </Items>
	            </Structure>
	            <Structure>
                      <Value Enum="New Temperature">new_temp</Value>
	              <Items>
	                <Item>new_temp</Item>
	              </Items>
	            </Structure>
                  </DiscreteInfo>
                </String>

                <String Name="pert_option" Label="Perturbation Theory Type" >
                  <DiscreteInfo DefaultIndex="0">
                    <Value Enum="First Order">first_order_perturbation_theory</Value>
                    <Value Enum="General">general_perturbation_theory</Value>
                  </DiscreteInfo>
	        </String>

              </ItemDefinitions>

            </Group>

            <Group Name="sens_calc" Label="Sensitivity Calculations" AdvanceLevel="0">

              <DetailedDescription>
                This group is used for sensitivity calculations on multi-group
                cross-sections calculated with mcc3. Sensitivity calculations can be
                calculated on the reference core or on a perturbed state.
              </DetailedDescription>

              <ItemDefinitions>

	        <String Name="sens_id" Label="ID">
                  <DiscreteInfo DefaultIndex="0">
                    <Value Enum="A">A</Value>
                    <Value Enum="B">B</Value>
                    <Value Enum="C">C</Value>
                    <Value Enum="D">D</Value>
                    <Value Enum="E">E</Value>
                    <Value Enum="F">F</Value>
                    <Value Enum="G">G</Value>
                    <Value Enum="H">H</Value>
                    <Value Enum="I">I</Value>
                    <Value Enum="J">J</Value>
                    <Value Enum="K">K</Value>
                    <Value Enum="L">L</Value>
                    <Value Enum="M">M</Value>
                    <Value Enum="N">N</Value>
                    <Value Enum="O">O</Value>
                    <Value Enum="P">P</Value>
                    <Value Enum="Q">Q</Value>
                    <Value Enum="R">R</Value>
                    <Value Enum="S">S</Value>
                    <Value Enum="T">T</Value>
                    <Value Enum="U">U</Value>
                    <Value Enum="V">V</Value>
                    <Value Enum="W">W</Value>
                    <Value Enum="X">X</Value>
                    <Value Enum="Y">Y</Value>
                    <Value Enum="Z">Z</Value>
                  </DiscreteInfo>
	        </String>

                <Int Name="sens_depletion_step" Label="Depletion Step">
                  <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
                  <DefaultValue>0</DefaultValue>
                </Int>

	        <String Name="sens_type" Label="Perturbation Type">
                  <DiscreteInfo DefaultIndex="0">
                    <Value Enum="keff">keff</Value>
                    <Value Enum="lambda_beta">lambda_beta</Value>
                  </DiscreteInfo>
	        </String>

	        <Void Name="sens_xs_updated" Label="Upate Cross Sections"
                      Optional="true" IsEnabledByDefault="true">
                  <BriefDescription>indicate whether cross-sections
                  will be recalculated for the perturbed state</BriefDescription>
                </Void>

                <Double Name="sensitivity_factor"
                        Label="Sensitivity Factor">
                  <BriefDescription>factor by which the cross-sections
                  are increased for the sensitivity analysis</BriefDescription>
                  <RangeInfo><Min Inclusive="false">0</Min></RangeInfo>
                  <DefaultValue>1.01</DefaultValue>
                </Double>

                <String Name="density_perturbation_factor_or_new_temp"
                        Label="Density Perturbation Factor or New Temperature"
                        Optional="true" IsEnabledByDefault="false">

                  <ChildrenDefinitions>

                    <Double Name="density_perturbation_factor"
                            Label="Density Perturbation Factor">
                      <BriefDescription>factor by which the density of the
                      isotopes perturbed will be multiplied</BriefDescription>
                      <RangeInfo><Min Inclusive="false">0</Min></RangeInfo>
                      <DefaultValue>1</DefaultValue>
                    </Double>

                    <Double Name="new_temp"
                            Label="New Temperature">
                      <BriefDescription>new temperature of the isotopes perturbed</BriefDescription>
                      <RangeInfo><Min Inclusive="false">0</Min></RangeInfo>
                      <DefaultValue>1</DefaultValue>
                    </Double>

	            <Void Name="xs_updated" Label="Upate Cross Sections"
                          Optional="true" IsEnabledByDefault="true"/>

                  </ChildrenDefinitions>

                  <DiscreteInfo DefaultIndex="0">
	            <Structure>
                      <Value Enum="Density Perturbation Factor">density_perturbation_factor</Value>
	              <Items>
		        <Item>density_perturbation_factor</Item>
		        <Item>xs_updated</Item>
	              </Items>
	            </Structure>
	            <Structure>
                      <Value Enum="New Temperature">new_temp</Value>
	              <Items>
	                <Item>new_temp</Item>
	              </Items>
	            </Structure>
                  </DiscreteInfo>
                </String>

                <String Name="pert_option" Label="Perturbation Theory Type" >
                  <DiscreteInfo DefaultIndex="0">
                    <Value Enum="First Order">first_order_perturbation_theory</Value>
                    <Value Enum="General">general_perturbation_theory</Value>
                  </DiscreteInfo>
	        </String>

              </ItemDefinitions>

            </Group>

          </ItemDefinitions>

        </Group>

      </ItemDefinitions>
    </AttDef>

  </Definitions>

</SMTK_AttributeSystem>
