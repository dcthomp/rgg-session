//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "smtk/simulation/pyarc/Registrar.h"

#include "smtk/common/Paths.h"
#include "smtk/common/PythonInterpreter.h"

#include "smtk/session/rgg/Resource.h"

#include "smtk/operation/RegisterPythonOperations.h"

#include "smtk/operation/groups/ExporterGroup.h"

namespace smtk
{
namespace simulation
{
namespace pyarc
{

void Registrar::registerTo(const smtk::operation::Manager::Ptr& operationManager)
{
  Registrar::addModuleToPythonPath();

  smtk::operation::registerPythonOperations(operationManager, "rggsession.simulation.pyarc.export_to_pyarc");

  smtk::operation::ExporterGroup(operationManager).registerOperation<smtk::session::rgg::Resource>(
    "rggsession.simulation.pyarc.export_to_pyarc.export_to_pyarc");

  smtk::operation::registerPythonOperations(operationManager, "rggsession.simulation.pyarc.generate_cross_sections");
}

void Registrar::unregisterFrom(const smtk::operation::Manager::Ptr& operationManager)
{
  operationManager->unregisterOperation("rggsession.simulation.pyarc.export_to_pyarc");
  operationManager->unregisterOperation("rggsession.simulation.pyarc.generate_cross_sections");
}

void Registrar::addModuleToPythonPath()
{
  std::string libDir = smtk::common::Paths::pathToLibraryContainingFunction(
    smtk::simulation::pyarc::Registrar::addModuleToPythonPath);
  smtk::common::PythonInterpreter::instance().addPathToPluginModule("rggsession", libDir);
}
}
}
}
