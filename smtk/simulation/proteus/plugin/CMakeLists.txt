set (ProteusPluginHdrs )
set (ProteusPluginSrcs )
smtk_add_plugin(
  smtkRGGSimulationProteusPlugin
  REGISTRAR smtk::simulation::proteus::Registrar
  MANAGERS smtk::operation::Manager
  PARAVIEW_PLUGIN_ARGS
    VERSION "1.0"
    REQUIRED_PLUGINS
      smtkCore
      smtkRGGSession
      smtkRGGSimulationProteus
    SOURCES
      ${ProteusPluginHdrs}
      ${ProteusPluginSrcs}
)

target_link_libraries(smtkRGGSimulationProteusPlugin
  PRIVATE
    smtkRGGSimulationProteus
  )
